# Builders Api v1
Este projeto contem a primeira versao para apreciacao de uma api REST onde se pode encontrar um 
CRUD de uma entidade.

`Entidade: Cliente`

# Documentacao:

Toda documentacao de ultilizacao da api pode ser consultada apos o start da aplicacao 
atravez do caminho:  http://localhost:8080/docs

Pode ser ultilizada a postman session (**PostManCollectionBuildersApi.json**)localizada na raiz da aplicacao para testar a api, basta importar no Postman.


# Start da aplicacao:

Para iniciar a aplicacao basta executar os comandos:

 `docker-compose build` em seguida `docker-compose up`


